# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

"""xdg desktop portals mock template"""

from test.templates import Request, ASVType, Session
from typing import Dict, List, Tuple, Iterator

import dbus.service

BUS_NAME = "org.freedesktop.portal.Desktop"
MAIN_OBJ = "/org/freedesktop/portal/desktop"
SYSTEM_BUS = False
MAIN_IFACE = "org.freedesktop.portal.RemoteDesktop"


def load(mock, parameters=None):
    # Delay before Request.response
    mock.delay: int = parameters.get("delay", 0)
    # Default response where none given
    mock.default_response: Tuple[int, ASVType] = parameters.get(
        "default-response", (0, {})
    )
    # Responses the methods, as a list, used up in-order
    responses: Dict[str, List[Tuple[int, ASVType]]] = parameters.get("responses", {})
    mock.responses: Dict[str, Iterator] = {m: iter(r) for m, r in responses.items()}

    mock.AddProperties(
        MAIN_IFACE,
        dbus.Dictionary(
            {
                "version": dbus.UInt32(parameters.get("version", 1)),
                "AvailableDeviceTypes": dbus.UInt32(
                    parameters.get("AvailableDeviceTypes", 0x7)
                ),
            }
        ),
    )

    mock.AddMethod(MAIN_IFACE, "NotifyPointerMotion", "oa{sv}dd", "", ""),


@dbus.service.method(
    MAIN_IFACE,
    sender_keyword="sender",
    in_signature="a{sv}",
    out_signature="o",
)
def CreateSession(self, options, sender):
    request = Request(bus_name=self.bus_name, sender=sender, options=options)

    try:
        response = next(self.responses["CreateSession"])
    except (KeyError, StopIteration):
        response = self.default_response

    res, results = response

    if res == 0:
        session = Session(bus_name=self.bus_name, sender=sender, options=options)
        results["session_handle"] = session.handle

    request.respond(res, results, delay=self.delay)

    return request.handle


@dbus.service.method(
    MAIN_IFACE,
    sender_keyword="sender",
    in_signature="oa{sv}",
    out_signature="o",
)
def SelectDevices(self, session_handle, options, sender):
    request = Request(bus_name=self.bus_name, sender=sender, options=options)

    try:
        response = next(self.responses["SelectDevices"])
    except (KeyError, StopIteration):
        response = self.default_response

    res, results = response

    request.respond(res, results, delay=self.delay)

    return request.handle


@dbus.service.method(
    MAIN_IFACE,
    sender_keyword="sender",
    in_signature="osa{sv}",
    out_signature="o",
)
def Start(self, session_handle, parent_window, options, sender):
    request = Request(bus_name=self.bus_name, sender=sender, options=options)

    try:
        response = next(self.responses["Start"])
    except (KeyError, StopIteration):
        response = self.default_response

    res, results = response

    request.respond(res, results, delay=self.delay)

    return request.handle
