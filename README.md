Portal Poker
============

`portal-poker` is a commandline utility to poke the DBus API(s) of XDG desktop
portals.

As a rule of thumb, the CLI looks like one of these two, depending whether you
want to talk to the portal or the impl of the portal:

```
$ portal-poker portal PortalName MethodName arg1 arg2
$ portal-poker impl PortalName MethodName arg1 arg2
```

For example:
```
$ portal-poker portal RemoteDesktop Start
$ portal-poker portal Wallpaper SetWallpaperURI file://home/$USER/Pictures/DesktopBackground.png
$ portal-poker portal Notification AddNotification --title "title" "notification-id" "body content"

$ portal-poker impl RemoteDesktop Start
$ portal-poker impl Wallpaper SetWallpaperURI org.example.AppId file://home/$USER/Pictures/DesktopBackground.png
$ portal-poker impl Notification AddNotification --title "title" org.example.AppId "notification-id" "body content"
```


# Tests

`portal-poker` comes with a test-suite, simply run `pytest` in the top build
directory to run test. See the `pytest --help` output for more information on
test selection, etc.

Note that the tests require python-dbusmock 0.27 or later and dbus-next with
a fix for this bug:
https://github.com/altdesktop/python-dbus-next/issues/113
