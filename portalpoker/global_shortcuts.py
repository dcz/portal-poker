# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from gi.repository import GLib
from .import Portal
import dbus
import click
import logging

logger = logging.getLogger("GlobalShortcuts")


@click.group(name="GlobalShortcuts")
@click.pass_context
def global_shortcuts(ctx):
    pp = ctx.obj
    import os
    pp.portal = Portal.from_string(
        bus=pp.bus,
        busname="org.gnome.Shell",
        objpath="/org/gnome/Shell",
        introspection=open(os.environ["PORTAL_FILE"]).read(),
    )


@global_shortcuts.command(name="ListShortcuts")
@click.pass_context
def list_shortcuts(ctx):
    pp = ctx.obj
    portal = pp.portal

    print(portal.method(
        "ListShortcuts",
        handle="",
        session_handle="",
        options=[],
    ))


@global_shortcuts.command(name="BindShortcuts")
@click.option(
    "--wait-seconds",
    type=int,
    help="Wait for N seconds for the shortcut signal",
    default=10,
)
@click.argument("accelerators", type=str, nargs=-1, required=True)
@click.pass_context
def bind_shortcuts(ctx, accelerators: str, wait_seconds: int):
    pp = ctx.obj
    portal = pp.portal

    results = portal.method(
        "BindShortcuts",
        handle="",
        session_handle="",
        shortcuts=[
            (
                a,
                {
                    'description': "Desc for {!r}".format(a),
                    'preferred_trigger': a,
                }
            )
            for a in accelerators
        ],
        parent_window="",
        options=[],
    )
    
    print(results)
    
    def on_activated(reply_action, parameters):
        if reply_action != action:
            return
        click.echo(f"Signal {reply_action}, {parameters}")
        pp.quit()
    
    portal.signal("AcceleratorActivated", on_activated)
    click.echo("Notification requested, waiting")
    pp.run(timeout=wait_seconds * 1000)


@global_shortcuts.command(name="GrabAccelerator")
@click.option(
    "--wait-seconds",
    type=int,
    help="Wait for N seconds for the shortcut signal",
    default=10,
)
@click.argument("accelerator", type=str)
@click.pass_context
def grab_accelertor(ctx, accelerator: str, wait_seconds: int):
    pp = ctx.obj
    portal = pp.portal

    action = portal.method(
        "GrabAccelerator",
        accelerator=accelerator,
        modeFlags=0xff,
        grabFlags=8,
    )
    
    
    def on_activated(reply_action, parameters):
        if reply_action != action:
            return
        click.echo(f"Signal {reply_action}, {parameters}")
        pp.quit()
    
    portal.signal("AcceleratorActivated", on_activated)
    click.echo("Notification requested, waiting")
    pp.run(timeout=wait_seconds * 1000)


@global_shortcuts.command(name="Test")
@click.option(
    "--wait-seconds",
    type=int,
    help="Wait for N seconds for the shortcut signal",
    default=10,
)
@click.pass_context
def test(ctx, wait_seconds: int):
    pp = ctx.obj
    portal = pp.portal

    accelerators = ['a', 'CTRL+b']
    
    session_handle = "";
    
    results = portal.method(
        "BindShortcuts",
        handle="",
        session_handle=session_handle,
        shortcuts=[
            (
                a,
                {
                    'description': "Desc for {!r}".format(a),
                    'preferred_trigger': a,
                }
            )
            for a in accelerators
        ],
        parent_window="",
        options=[],
    )

    print("LIST-----------")
    print(portal.method(
        "ListShortcuts",
        handle="",
        session_handle=session_handle,
        options=[],
    ))
    
    def on_activated(handle, shortcut, timestamp, options):
        if handle != session_handle:
            return
        click.echo(f"Activated {shortcut} at {timestamp}, {options}")
        #pp.quit()
    
    def on_deactivated(handle, shortcut, timestamp, options):
        if handle != session_handle:
            return
        click.echo(f"DeActivated {shortcut} at {timestamp}, {options}")
        #pp.quit()
    
    portal.signal("Activated", on_activated)
    portal.signal("Deactivated", on_deactivated)
    click.echo("Notification requested, waiting")
    pp.run(timeout=wait_seconds * 1000)

@global_shortcuts.command(name="TestUnbind")
@click.option(
    "--wait-seconds",
    type=int,
    help="Wait for N seconds for the shortcut signal",
    default=10,
)
@click.pass_context
def test(ctx, wait_seconds: int):
    pp = ctx.obj
    portal = pp.portal

    accelerators = ['a', 'CTRL+b']
    
    session_handle = "";
    
    results = portal.method(
        "BindShortcuts",
        handle="",
        session_handle=session_handle,
        shortcuts=[
            (
                a,
                {
                    'description': "Desc for {!r}".format(a),
                    'preferred_trigger': a,
                }
            )
            for a in accelerators
        ],
        parent_window="",
        options=[],
    )
    
    results = portal.method(
        "BindShortcuts",
        handle="",
        session_handle=session_handle,
        shortcuts=[
            (
                a,
                {
                    'description': "Desc for {!r}".format(a),
                    'preferred_trigger': a,
                }
            )
            for a in accelerators
        ],
        parent_window="",
        options=[],
    )

    print("LIST-----------")
    print(portal.method(
        "ListShortcuts",
        handle="",
        session_handle=session_handle,
        options=[],
    ))
    
    def on_activated(handle, shortcut, timestamp, options):
        if handle != session_handle:
            return
        click.echo(f"Activated {shortcut} at {timestamp}, {options}")
        #pp.quit()
    
    def on_deactivated(handle, shortcut, timestamp, options):
        if handle != session_handle:
            return
        click.echo(f"DeActivated {shortcut} at {timestamp}, {options}")
        #pp.quit()
    
    portal.signal("Activated", on_activated)
    portal.signal("Deactivated", on_deactivated)
    click.echo("Notification requested, waiting")
    pp.run(timeout=wait_seconds * 1000)
