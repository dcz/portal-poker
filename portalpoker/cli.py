#!/usr/bin/env python3
#
# SPDX-License-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional

from portalpoker import PortalPoker, PortalError

# Portal-specific click commands
from portalpoker.background import background, impl_background
from portalpoker.global_shortcuts import global_shortcuts
from portalpoker.networkmonitor import networkmonitor
from portalpoker.notification import notification, impl_notification
from portalpoker.remotedesktop import remotedesktop, impl_remotedesktop
from portalpoker.wallpaper import wallpaper, impl_wallpaper

import click
import logging
import sys

logger = None


def _init_logger(verbose: int) -> None:
    if verbose >= 1:
        lvl = logging.DEBUG
    else:
        lvl = logging.INFO

    logging.basicConfig(format="%(levelname).1s| %(name)s: %(message)s", level=lvl)


@click.group()
@click.option("--verbose", "-v", count=True, help="Enable debug logging")
@click.pass_context
def portal_poker(ctx, verbose: int):
    """
    portal-poker is a CLI utility to poke at XDG desktop portals, including
    calling methods on the portals. Requests and Sessions are handled
    as-required with each command typically creating a session (if needed) and
    a request (if needed) for the method invocation.

    This tool is primarily focused at debugging and testing the portal
    implementations.

    Invocation of a method on a portal is generally:

        $ portal-poker portal PortalName MethodName arg1 arg2

    Invocation of a method on an impl.portal is generally:

        $ portal-poker impl PortalName MethodName arg1 arg2

    """
    global logger

    _init_logger(verbose)
    logger = logging.getLogger("portal-poker")


@portal_poker.command(name="info")
@click.option(
    "--impl",
    is_flag=True,
    help="Use the impl interface of the given portal. Ignored for a fully qualified name.",
)
@click.option(
    "--busname",
    type=str,
    default="org.freedesktop.portal.Desktop",
    help="The portal bus name",
)
@click.argument("portal", type=str)
@click.pass_context
def portal_poker_info(ctx, impl: bool, busname: str, portal: str):
    """
    Show information about the given portal

    The portal can be specified either with just the name (e.g.
    "RemoteDesktop") or with the fully qualified name (e.g.
    "org.freedesktop.portal.RemoteDesktop").
    """
    if not portal.startswith("org.freedesktop"):
        if "." in portal:
            raise ValueError(
                f"Invalid name {portal}, use either fully qualified name or just the portal name"
            )
        if impl:
            portal = f"org.freedesktop.impl.portal.{portal}"
        else:
            portal = f"org.freedesktop.portal.{portal}"

    try:
        pp = PortalPoker.create(busname=busname)
        # Print the version of the portal first
        click.echo(pp.show_portal_info(portal))
    except PortalError as e:
        click.secho(f"Portal error: {e}")
        sys.exit(1)


@portal_poker.group(name="portal")
@click.option(
    "--busname",
    type=str,
    default="org.freedesktop.portal.Desktop",
    help="The portal bus name",
)
@click.pass_context
def portal_poker_portal(ctx, busname: str):
    """
    Run a command against an XDG Portal
    """
    try:
        ctx.obj = PortalPoker.create(busname=busname)
    except PortalError as e:
        click.secho(f"{e}", fg="red")
        sys.exit(1)


@portal_poker.group(name="impl")
@click.option(
    "--busname",
    type=str,
    help="The portal name",
)
@click.pass_context
def portal_poker_impl(ctx, busname: Optional[str]):
    """
    Run a command against an impl XDG Portal
    """
    if busname:
        try:
            ctx.obj = PortalPoker.create(busname=busname)
        except PortalError as e:
            click.secho(f"{e}", fg="red")
            sys.exit(1)


portal_poker_portal.add_command(background)
portal_poker_portal.add_command(global_shortcuts)
portal_poker_portal.add_command(networkmonitor)
portal_poker_portal.add_command(notification)
portal_poker_portal.add_command(remotedesktop)
portal_poker_portal.add_command(wallpaper)

portal_poker_impl.add_command(impl_background)
portal_poker_impl.add_command(impl_notification)
portal_poker_impl.add_command(impl_remotedesktop)
portal_poker_impl.add_command(impl_wallpaper)

if __name__ == "__main__":
    portal_poker()
